//
//  main.m
//  Soundwave
//
//  Created by jemmyphan on 11/09/2017.
//  Copyright (c) 2017 jemmyphan. All rights reserved.
//

@import UIKit;
#import "SoundwaveAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SoundwaveAppDelegate class]));
    }
}
